- $\ddx \sin x = \cos x$
- $\ddx \sinh x = \cosh x$, and vice versa
- $\int \frac1u \ du = \ln |u|$
- Position vector
    - Velocity: 1st d/dx
    - Speed: Magnitude of 1st
    - Second derivative: 2nd d/dx
- Length of curve $\vec r(t)$ (distance travelled) is integral of magnitude $||\dot{\vec r}||$
- Dimensions of largest box: Lagrange $\langle S_x, S_y, S_z \rangle = \lambda \langle V_x, V_y, V_z \rangle$ 2 formulas
- Vector field is **conservative** if:
    - 2D: $\frac{dQ}{dx} = \frac{dP}{dy},$
    - 3D: $\text{curl} \vec F = 0$
- **Green's**:
    - $\underset{C}{\oint} P \ dx + Q \ dy = \underset{D}{\iint} \left(\frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y}\right) \ dA$
    - $A = \frac12 \underset{C}{\oint} x \ dy - y \ dx = \frac12 \left[ \int xy' \ dt - \int yx' \ dt \right] \\[.15em]$
- **Area**: $A = \underset{D}{\iint} || \vec r_u \times \vec r_v || \ dA$
- Unit circle $(\cos x, \sin x)$
    - $\frac\pi3 = \left(\frac12, \frac{\sqrt 3}2\right)$
    - $\frac\pi4 = \left(\frac{\sqrt 2}2, \frac{\sqrt 2}2\right)\\[.15em]$
- **Sphere parametrization**: $\int_c^d \int_\alpha^\beta \int_a^b f(\rho \sin \phi \cos \theta, \ \rho \sin \phi \sin \theta, \ \rho \cos \phi) \ \rho^2 \sin \phi \ d\rho \ d\theta \ d\phi$
- **Tangent plane**:
    - $\vec n = \vec r_x \times \vec r_y$
    - $a(x - x_0) + b(y - y_0) + c(z - z_0) = 0 \\[.15em]$
- **Stokes'**: $\underset{C}{\int} \vec F \cdot d \vec r = \underset{S}{\iint} \text{curl}\vec F \cdot d \vec S$
- $\underset{S}{\iint} \vec F \cdot d \vec S = \iint \vec F(\vec r(x, y)) \cdot \plusmn (\vec r_x \times \vec r_y) \ dy \ dx$
    - **Divergence**: $\underset{S} {\iint} \vec F \cdot d\vec S = \underset{E}{\iiint} \text{div} \vec F \ dV$
- **Jacobian**: $x = g(u, v)$ and $y = h(u, v$
    - $\frac{\partial (x, y)}{\partial (u, v)} =
\Large\left|
\begin{matrix}
\frac{\partial x}{\partial u} & \frac{\partial x}{\partial v} \\
\frac{\partial y}{\partial u} & \frac{\partial y}{\partial v} \\
\end{matrix}
\right|$

<img src="../../Chapter%2013/13.2.1.png" width="400">

- Useful tips:
    - Plug things in more often