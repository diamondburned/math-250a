- Study the unit circle, $\frac{\sqrt3}{2}.$

---

Distance formula:

$$
\begin{aligned}
\sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}
\end{aligned}
$$

---

rho theta phi:

$$
\begin{aligned}
\int_c^d \int_\alpha^\beta \int_a^b f(\rho \sin \phi \cos \theta, \ \rho \sin \phi \sin \theta, \ \rho \cos \phi) \ \rho^2 \sin \phi \ d\rho \ d\theta \ d\phi
\end{aligned}
$$

---

$$
\begin{aligned}
\int \frac1x \ dx &= \ln |x| \\
\int \sin kx \ dx &= -\frac1x \cos kx \\
\int \cos kx \ dx &= \frac1x \sin kx \\
\end{aligned}
$$