# Stokes' Theorem

![](https://tutorial.math.lamar.edu/Classes/CalcIII/StokesTheorem_Files/image001.png)

Curve $C$ is the **boundary curve**. Surface $S$ induces the **positive orientation of $C$**.

$$
\begin{aligned}
\underset{C}{\int} \vec F \cdot d \vec r = \underset{S}{\iint} \text{curl}\vec F \cdot d \vec S
\end{aligned}
$$