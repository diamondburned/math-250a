# Parametric Surfaces

Finding the tangent plane given $\vec r$ and $(x_0, y_0, z_0)$:

$$
\begin{aligned}
\vec n = \vec r_u \times \vec r_v
\end{aligned}
$$

Then

- plug the point into parts of the vector to find $u$ and $v$,
- plug $u$ and $v$ into $\vec n$,
- plug its parts into $(x - x_0)$ and so on.

---

To find the area:

$$
\begin{aligned}
A = \underset{D}{\iint} || \vec r_u \times \vec r_v || \ dA
\end{aligned}
$$