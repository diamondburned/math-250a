Jacobian transformation for $x = g(u, v)$ and $y = h(u, v)$:

$$
\begin{aligned}
\frac{\partial (x, y)}{\partial (u, v)} =
\Large\left|
\begin{matrix}
\frac{\partial x}{\partial u} & \frac{\partial x}{\partial v} \\
\frac{\partial y}{\partial u} & \frac{\partial y}{\partial v} \\
\end{matrix}
\right|
\end{aligned}
$$