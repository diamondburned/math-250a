# Minimum and Maximum

Obvious definitions:

1. A function has a **relative minimum** at $(a, b)$ if $f(x, y) \geq f(a, b)$ for all $(x, y)$ in
region around $(a, b)$.
2. A function has a **relative maximum** at $(a, b)$ if $f(x, y) \leq f(a, b)$ for all $(x, y)$ in
region around $(a, b)$.
3. $(a, b)$ is a **critical point** of $f(x, y)$ if either:
   - $\nabla f(a, b) =  \vec 0$ (i.e. $f_x(a, b) = 0$ and $f_y(a, b) = 0$), or
   - $f_x(a, b)$ and/or $f_y(a, b)$ doesn't exist.

Suppose that $(a, b)$ is a critical point of $f(x, y)$, and that the second order partial
derivatives are continuous in some region that contains $(a,b)$. Define:

$$
\begin{aligned}
D = D(a, b) = f_{xx} (a, b) \cdot f_{yy} (a, b) - \left[f_{xy} (a, b)\right]^2
\end{aligned}
$$

We then have:

- $D > 0$ and $f_{xx} > 0$, then it's a **relative minimum**.
- $D > 0$ and $f_{xx} < 0$, then it's a **relative maximum**.
- $D < 0$, then it's a **saddle point**. (Apparently if $f_{xx} = 0$, it's also a saddle point.)
- $D = 0$, then it may be any.

To find all extremas:

1. Find the first and second order partial derivatives.
2. Find critical points (set first p. derivatives to $0$, then solve for $(x, y)$).
3. Plug second p. derivatives into the $D$ formula above.
4. ???
5. Profit.

---

To find the distance of a plane from point $(a, b, c)$, use:

$$
\begin{aligned}
d = \sqrt{(x-a)^2 + (y-b)^2 + (z-c)^2}
\end{aligned}
$$

Then, get the equation in terms of $z$ and plug it into $d$. Since we're differentiating $d$ which
has a square root, we can instead differentiate $d^2$ for the same minimum values.

---

- A region $\R^2$ is **closed** if it includes its boundary, or **open** if it doesn't.
    - *Note*:  closed basically means to include its boundaries, e.g. $3 \leq a \leq 5$, as opposed
    to open, e.g. $3 < a < 5$.
- A region $\R^2$ is **bounded** if it can be completely contained in a disk (i.e. finite).

(TODO: omit Extreme Value Theorem if not needed.)

If $f(x, y)$ is continuous in some *closed, bounded* $D$ in $\R^2$, then there are points
$f(x_1, y_1)$ being the abs. max. and $f(x_2, y_2)$ being the abs. min. of the function in $D$.


Finding Absolute Extrema:
1. Find all the critical points of the function that lie in region $D$ and determine the function
value at each of these points.
2. Find all extrema of the function on the boundary. (Calc 1 approach.)
3. The largest and smallest values found in the first two steps are the absolute minimum and
maximum of the function.